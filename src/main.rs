
use common_failures::prelude::*;

use common_failures::quick_main;

use std::collections::HashMap;
use std::collections::hash_map;
use std::cmp;
use std::time::Instant;
use std::path::Path;

use clap::{Arg, App};

use failure::format_err;

use image::{
    Rgba, 
    GenericImageView,
    SubImage,
};

#[derive(Debug, PartialEq)]
struct BoundingBox {
    min: (u32, u32),
    max: (u32, u32),
}

impl BoundingBox {
    pub fn new() -> BoundingBox {
        BoundingBox {
            min: (u32::max_value(), u32::max_value()),
            max: (u32::min_value(), u32::min_value()),
        }
    }

    pub fn update(&mut self, x: u32, y: u32) {
        self.min.0 = cmp::min(self.min.0, x);
        self.min.1 = cmp::min(self.min.1, y);
        self.max.0 = cmp::max(self.max.0, x);
        self.max.1 = cmp::max(self.max.1, y);
    }

    pub fn crop<'a, I>(&self, image: &'a I) -> SubImage<&'a I::InnerImageView> 
        where I: GenericImageView,
              I: 'a,
    {
        let origin = self.top_left();
        let dimension = self.dimension();

        image.view(origin.0, origin.1, dimension.0, dimension.1)

    }

    pub fn dimension(&self) -> (u32, u32) {
        ((self.max.0 - self.min.0) + 1, (self.max.1 - self.min.1) + 1)
    }

    pub fn top_left(&self) -> &(u32, u32) {
        &self.min
    }
}

type Color = Rgba<u8>;

#[derive(Debug, PartialEq)]
struct ColorAreas {
    boxes: HashMap<Color, BoundingBox>,
}

impl ColorAreas {
    pub fn new() -> ColorAreas {
        ColorAreas {
            boxes: HashMap::new()
        }
    }

    pub fn add_pixel(&mut self, x: u32, y: u32, color: Color) {
        let bounding_box = self.boxes
            .entry(color)
            .or_insert_with(BoundingBox::new);
        
        bounding_box.update(x, y);
    }

    pub fn bounding_boxes(&self) -> hash_map::Values<Color, BoundingBox> {
        self.boxes.values()
    }

    pub fn num_boxes(&self) -> usize {
        self.boxes.len()
    }

    fn get_boxes_mut(&mut self) -> hash_map::IterMut<Color, BoundingBox> {
        self.boxes.iter_mut()
    }
}

fn extract_areas(source_path: &dyn AsRef<Path>) -> Result<ColorAreas> {

    let start_time = Instant::now();

    let mask_image = image::open(source_path)?;
    println!("Time loading mask: {:?}", Instant::now().duration_since(start_time));

    let mut color_areas = ColorAreas::new();

    let start_time = Instant::now();
    for (x, y, color) in mask_image.pixels() {
        color_areas.add_pixel(x, y, color);
    }
    println!("Time finding bounding boxes: {:?}", Instant::now().duration_since(start_time));

    Ok(color_areas)
}

fn extract_areas_grid(source_path: &dyn AsRef<Path>, grid: u32) -> Result<ColorAreas> {

    let start_time = Instant::now();
    let mask_image = image::open(source_path)?;
    let mask_image = mask_image.as_rgba8().ok_or(format_err!("not an rgba8 picture"))?;
    println!("Time loading mask: {:?}", Instant::now().duration_since(start_time));

    let mut color_areas = ColorAreas::new();

    let start_time = Instant::now();
    for y in (0..mask_image.height()).step_by(grid as usize) {
        for x in (0..mask_image.width()).step_by(grid as usize) {
            let color = mask_image.get_pixel(x, y);
            color_areas.add_pixel(x, y, *color);
        }
    }

    for (color, bounding_box) in color_areas.get_boxes_mut() {
        let mut min_x = bounding_box.min.0;
        let mut min_y = bounding_box.min.1;

        let mut max_x = bounding_box.max.0;
        let mut max_y = bounding_box.max.1;

        for x in (0..min_x).rev() {
            let pixel_color = mask_image.get_pixel(x, bounding_box.min.1);
            if color != pixel_color {
                min_x = x + 1;
                break;
            }
        }

        for y in (0..min_y).rev() {
            let pixel_color = mask_image.get_pixel(bounding_box.min.0, y);
            if color != pixel_color {
                min_y = y + 1;
                break;
            }
        }

        for x in max_x .. mask_image.width() + 1 {
            if x == mask_image.width() {
                max_x = x - 1;
                break;
            }
            let pixel_color = mask_image.get_pixel(x, bounding_box.max.1);
            if color != pixel_color {
                max_x = x - 1;
                break;
            }
        }

        for y in max_y .. mask_image.height() + 1 {
            if y == mask_image.height() {
                max_y = y - 1;
                break;
            }
            let pixel_color = mask_image.get_pixel(bounding_box.max.0, y);
            if color != pixel_color {
                max_y = y - 1;
                break;
            }
        }

        bounding_box.min = (min_x, min_y);
        bounding_box.max = (max_x, max_y);
    }

    println!("Time finding bounding boxes: {:?}", Instant::now().duration_since(start_time));

    Ok(color_areas)
}

fn crop_picture(color_areas: &ColorAreas, original_file: &dyn AsRef<Path>, name_prefix: &dyn AsRef<str>, format: &dyn AsRef<str>, offset: usize) -> Result<usize> {

    let mut i = offset;

    let start_time = Instant::now();
    let original_image = image::open(original_file)?;
    println!("Time loading original: {:?}", Instant::now().duration_since(start_time));

    let start_time = Instant::now();
    for bounding_box in color_areas.bounding_boxes() {

        let bb_dimension = bounding_box.dimension();
        if bb_dimension.0 == original_image.width() && bb_dimension.1 == original_image.height() {
            continue;
        }

        let cropped_view = bounding_box.crop(&original_image);

        let new_file_name = format!("{}_{:03}.{}",name_prefix.as_ref(), &i, format.as_ref());

        cropped_view.to_image().save(new_file_name)?;

        i+=1;
    }

    println!("Time writing cropped pictures: {:?}", Instant::now().duration_since(start_time));
    
    Ok(i-offset)
}

fn run() -> Result<()> {

    let app = App::new("BBCut")
        .arg(Arg::with_name("mask")
             .value_name("MASK FILE")
             .required(true)
             )
        .arg(Arg::with_name("original")
             .value_name("ORIGINAL FILE")
             .required(true)
             )
        .arg(Arg::with_name("name_prefix")
             .short("p")
             .long("prefix")
             .value_name("PREFIX")
             )
        .arg(Arg::with_name("format")
             .short("f")
             .long("format")
             .value_name("FORMAT")
             )
        .arg(Arg::with_name("offset")
             .short("o")
             .long("offset")
             .value_name("OFFSET")
             )
        .arg(Arg::with_name("grid")
             .long("grid")
             .value_name("GRID")
             )
        .get_matches();

    let mask_file = app.value_of("mask").unwrap();
    let original_file = app.value_of("original").unwrap();
    let name_prefix = app.value_of("name_prefix").unwrap_or("crop");
    let format = app.value_of("format").unwrap_or("png");
    let offset: usize = app.value_of("offset").unwrap_or("0").parse()?;

    let start_time = Instant::now();
    let color_areas = if let Some(grid) = app.value_of("grid") {
        let grid: u32 = grid.parse()?;
        extract_areas_grid(&mask_file, grid)
    } else {
        extract_areas(&mask_file)
    }?;

    println!("creating up to {} files", color_areas.num_boxes());

    let n = crop_picture(&color_areas, &original_file, &name_prefix, &format, offset)?;

    println!("Created {} files in {:?}", n, Instant::now().duration_since(start_time));

    Ok(())
}

quick_main!(run);


#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn test_gridded() {
        let path = "resources/mask.png";

        let areas1 = extract_areas(&path).unwrap();
        let areas2 = extract_areas_grid(&path, 48).unwrap();

        for (color, area) in areas1.boxes.iter() {
            let area2 = &areas2.boxes[color];
            println!("areas: {:?}, {:?}, {:?}", color, area, area2);
            assert_eq!(area, area2);
        }
    }

}
