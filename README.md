Program that finds the boundingbox for each color in a given "mask" image.

For each found bounding box it will then cut out all bounding boxes of a "original" image and put them in separate files.
